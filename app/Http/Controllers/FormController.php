<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('tampilan.form');
    }

    public function kirim(Request $Request){
        // dd($Request->all());
        $nama = $Request['nama'];
        $alamat = $Request['addres'];
        return view('tampilan.home', compact('nama','alamat'));
    }
}
