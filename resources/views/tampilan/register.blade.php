@extends('layout.master')

@section('judul')
<h1>Halaman Formulir</h1>
@endsection

@section('content')
    
    <h3>Buat Account Baru</h3>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method= "post">
        @csrf
        <label for="">First Name: </label> <br> <br>
        <input type="text" name="nama_depan">
        <br>
        <br>
        <label for="">Last Name: </label> <br> <br>
        <input type="text" name="nama_belakang">
        <p>Gender</p>
        <input type="radio" name="gender" value="male"> Male <br>
        <input type="radio" name="gender" value="fimale"> Fimale <br> <br>
        <label>Nationaly</label> <br> <br>
        <select name="nationaly" value="nationaly" id="">
            <option value="7">Indonesia</option>
            <option value="8">Malaysia</option>
            <option value="9">Singapure</option>
        </select>
        <br>
        <br>
        <label for=""> Language Spoken</label> <br> <br>
        <input type="checkbox"  name="bahasa" value="bahasa"> Bahasa Indonesia <br>
        <input type="checkbox"  name="bahasa" value="bahasa" > English <br>
        <input type="checkbox"  name="bahasa" value="bahasa"> Other <br> <br>

        <label for="">Bio</label> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br> <br>
        <input type="submit" value="kirim">
    </form>

    @endsection